import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, NgModel } from '@angular/forms';

import { AppComponent } from './app.component';
import { ListaCitasComponent } from './lista-citas/lista-citas.component';
import { CrearCitaComponent } from './crear-cita/crear-cita.component';


@NgModule({
  declarations: [
    AppComponent,
    ListaCitasComponent,
    CrearCitaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
