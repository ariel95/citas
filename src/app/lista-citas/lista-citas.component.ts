import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-lista-citas',
  templateUrl: './lista-citas.component.html',
  styleUrls: ['./lista-citas.component.css']
})
export class ListaCitasComponent implements OnInit {
  @Input() listadoCitas: any;
  @Output() emitEliminarCita = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
  }

  eliminarCita(i: number) {
    this.emitEliminarCita.emit(i);
  }

}
