import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  listCitas: any[] = [];

  agreagarCitas(cita) {
    this.listCitas.push(cita);
    console.log('desde el padre');
    console.log(cita);
  }
  eliminarCitaListado(i: number) {
    this.listCitas.splice(i);
  }
}
