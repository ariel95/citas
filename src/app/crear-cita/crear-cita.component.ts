import { Component, EventEmitter, OnInit, Output } from '@angular/core';


@Component({
  selector: 'app-crear-cita',
  templateUrl: './crear-cita.component.html',
  styleUrls: ['./crear-cita.component.css']
})
export class CrearCitaComponent implements OnInit {

  nombre = '';
  fecha = '';
  hora = '';
  sintoma = '';

  formularioIncorrecto = false;
  @Output() nuevaCita = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
  }

  agregarCita() {
    if (this.nombre === '' || this.fecha === '' || this.hora === '' || this.sintoma === '') {
      this.formularioIncorrecto = true;
      return;
    }
    this.formularioIncorrecto = false;
    const CITAS = {
      nombre: this.nombre,
      fecha : this.fecha,
      hora : this.hora,
      sintoma: this.sintoma
    };

    this.nuevaCita.emit(CITAS);

    this.resetCampos();
  }

  resetCampos() {
    this.nombre = '';
    this.fecha = '';
    this.hora = '';
    this.sintoma = '';
  }
}
